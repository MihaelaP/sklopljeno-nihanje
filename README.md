# README #


### Čemu je ta repozitorij namenjen? ###

* Projektu Java pri predmetu Programiranje 2
* Moj projekt: Sklopljeno nihanje

### Kratek opis projekta ###

Osnovna verzija:

* Program simulira nihanje nihala , sestavljenega iz dveh enakih nihal povezanih s prožno vzmetjo:

* Nihanje simuliramo z numerično metodo

* Animacija sproti prikazuje stanje

* Program animira nihanje na zaslonu.

Razširitve:

* Model razširimo tako, da sklopimo n nihal.

* Program omogoča interakcijo z uporabnikom (npr. uporabnik nastavirazdaljo med sosednjima nihaloma, koeficient vzmeti)

### Več informacij... ###

* mihaela.pusnik@student.fmf.uni-lj.si