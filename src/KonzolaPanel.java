import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class KonzolaPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private MehanicniModel[] fireworks = new MehanicniModel[1];
	//Nastavimo spremenljivke
	
	private double dolzina = 0.5;
	private double masa = 0.5;
	private double koeficient = 1;
	private int stevilo = 10;
	private boolean leva = true;
	private boolean desna = true;
	
	
	
	// Funkcije za klic vhodnih podatkov, ki jih vnese uporabnik.
	public void getDolzina(double dolzina) {
		this.dolzina = dolzina;
	}
	
	public void getMasa(double masa) {
		this.masa = masa;
	}
	
	public void getKoeficient(double koeficient) {
		this.koeficient = koeficient;
	}
	
	public void getStevilo(int stevilo) {
		this.stevilo = stevilo;
	}

	public void getLeva(boolean leva) {
		this.leva = leva;
	}
	
	public void getDesna(boolean desna) {
		this.desna = desna;
	}

	
		
	KonzolaPanel() {
		init();
	}
	

	
	
	void init() {
	
		// Osvežitev, ki jih uporabnik vnese preko okenca.
		boolean leva = this.leva;
		boolean desna = this.desna;
		
		int n = this.stevilo; 
		double stena1 = 0.35;
		double stena2 = 1.25;
		
		double[] x = new double[n];
		for(int i = 0; i<n ; i++){
			x[i] = stena1 + (stena2-stena1)*(i*i+1)/(n*n+1);
		}
		
		double[] m = new double[n];
		for(int i = 0; i<n ; i++){
			m[i] = this.masa;
		}
		double[] k = new double[n+1];
		for(int i = 0; i<n+1 ; i++){
			k[i] = this.koeficient;
		}
		
		double[] L = new double[n+1];
		for(int i = 0; i<n+1 ; i++){
			L[i] = this.dolzina;
		}
		if (!leva){k[0]=0; L[0]=0;}
		if (!desna){k[n]=0; L[n]=0;}
		
		double[] v = new double[n];
		
		// Klic naslednjega koraka Metode Runge Kutta.
		fireworks[0] = new MehanicniModel( x, v, k, m , stena1, stena2, L, this.getHeight(), this.getWidth(), leva, desna );
	}
	public void reset() {
		init();
	}
	public void paint (Graphics g) {
		// Izris slike.
		super.paintComponent(g);
		fireworks[0].draw(g);
		}
}
