import java.awt.Color;
import java.awt.Graphics;
public class MehanicniModel {
	// Nastavimo parametre modela
	private double[] v; // hitrosti utezi
	private double[] x; // odmiki
	private double[] k; // koeficient vzmeti
	private double[] m; // masa utezi
	private double stena1; // lega leve stene
	private double stena2; // lega desne stene
	private double[] L; // dolzina neraztegnjenih vzmeti
	private int n; // stevilo kroglic
	boolean leva; // pove, če imamo levo steno
	boolean desna; // pove, če imamo desno steno
	
	Color color;
	
	
	
	MehanicniModel(double[] x, double[] v, double [] k, double [] m, double stena1, double stena2, double[] L, int height, int width, boolean leva, boolean desna) {
		this.x = x;
		this.v = v;	
		this.k = k;
		this.m = m;
		this.stena1 = stena1;
		this.stena2 = stena2;
		this.L = L;
		this.leva = leva;
		this.desna = desna;
	    color = new Color((int)(Math.random() * 0xFFFFFF));
	}
	
	
	
	
	void draw(Graphics g) {
		// Določimo barvo.
		Color drawColor = color;
		g.setColor(drawColor);
		
		//Določimo velikost slike.
		int height = 500;
		int width = 500;
		
		// Izberemo 0.04, ker se slika osvežuje s frekvenco 25 HZ (tako deluje Timer).
		double h = 0.04;
		
		
		// Runge Kutta metoda za izračun naslednja koraka simulacije.
		int n = x.length;
		double x1;
		double k11;
		double L1;
		if (n>1){
			 x1 = x[1];
			 k11 = k[1];
			 L1 = L[1];
		}
		else { 
			x1 = stena2;
			if (!this.desna){
				k11=0;
				L1=0;
			}
			else{
				k11 = k[0];
				L1 = L[0];
			}
			}

		
		double[] a = new double[n];
		a[0] = +  k11 * (x1 - x[0] - L1)/ m[0]
				- k[0]  * ( x[0] - stena1 - L[0] )/ m[0];	
		
		if (n>1){
		a[n-1] = - k[n-1]  * (x[n-1] - x[n-2] - L[n-1] )/m[n-1]
				 + k[n] * ( stena2 - x[n-1] -L[n])/m[n-1];	
		}
		
		for (int i = 1; i < n - 1; i++){	
			a[i] = - k[i]   * (x[i] - x[i-1] - L[i])/m[i]
					+ k[i+1] * (x[i+1] - x[i] - L[i])/m[i];
		}
	//Koeficienti Runge Kutta metode.
		double[] k0 = new double[n];
	    double[] k1 = new double[n];
	    double[] k2 = new double[n];
	    double[] k3 = new double[n];
	    double[] l0 = new double[n];
	    double[] l1 = new double[n];
	    double[] l2 = new double[n];
	    double[] l3 = new double[n];
		
		for(int i=0; i< n ; i++){
			 k0[i] =   v[i];
			 l0[i] =   a[i];
		}
		double[] estimatex = new double[n];
		double[] estimatev = new double[n];
		for(int i = 0; i<n ; i++) {
		      estimatex[i] = x[i] + h * k0[i]/2;
		      estimatev[i] = v[i] + h * l0[i]/2;
		    }

		
		double estimatex1;
		if (n>1){
			 estimatex1 = estimatex[1];
		}
		else { 
			estimatex1 = stena2;
			}
		
		
		
		a[0] = + k[1]*(estimatex1 - estimatex[0] - L[1])/ m[0]
			- k[0]*( estimatex[0] - stena1 - L[0])/m[0];	
		
		if (n>1){
	    a[n-1] = -  k[n-1]*( estimatex[n-1] -  estimatex[n-2] - L[n-1] )/m[n-1]
	    		+ k[n]* (stena2 - estimatex[n-1]-L[n])/m[n-1];	
		}
		
	    for (int i = 1; i < n - 1; i++){	
	    	a[i] = -  k[i]* ( estimatex[i] - estimatex[i-1] - L[i])/m[i]
	    			+ k[i+1]*( estimatex[i+1] - estimatex[i] - L[i+1])/m[i];
	    }
		
	    for(int i =0; i<n; i++){
	    	k1[i] = estimatev[i];
	    	l1[i] = a[i];
	    }
		
	    
	    for(int i = 0; i<n ; i++) {
		      estimatex[i] = x[i] + h * k1[i]/2;
		      estimatev[i] = v[i] + h * l1[i]/2;
		    }
		
		if (n>1){
			 estimatex1 = estimatex[1];
		}
		else { 
			estimatex1 = stena2;
			}
		
	    a[0] = + k[1]*(estimatex1 - estimatex[0] - L[1])/ m[0]
				- k[0]*( estimatex[0] - stena1 - L[0])/m[0];	
	    if (n>1){
		    a[n-1] = -  k[n-1]*( estimatex[n-1] -  estimatex[n-2] - L[n-1] )/m[n-1]
		    		+ k[n]* (stena2 - estimatex[n-1]-L[n])/m[n-1];	
	    }
	    
		    for (int i = 1; i < n - 1; i++){	
		    	a[i] = -  k[i]* ( estimatex[i] - estimatex[i-1] - L[i])/m[i]
		    			+ k[i+1]*( estimatex[i+1] - estimatex[i] - L[i+1])/m[i];
		    }

	    for(int i =0; i<n; i++){
	    	k2[i] = estimatev[i];
	    	l2[i] = a[i];
	    }    
	    
	    for(int i = 0; i<n ; i++) {
		      estimatex[i] = x[i] + h * k2[i];
		      estimatev[i] = v[i] + h * l2[i];
		    }
		
	    if (n>1){
			 estimatex1 = estimatex[1];
		}
		else { 
			estimatex1 = stena2;
			}
	    
	    a[0] = + k[1]*(estimatex1 - estimatex[0] - L[1])/ m[0]
				- k[0]*( estimatex[0] - stena1 - L[0])/m[0];
	    
	    if(n>1){
		    a[n-1] = -  k[n-1]*( estimatex[n-1] -  estimatex[n-2] - L[n-1] )/m[n-1]
		    		+ k[n]* (stena2 - estimatex[n-1]-L[n])/m[n-1];
	    }
	    
		    for (int i = 1; i < n - 1; i++){	
		    	a[i] = -  k[i]* ( estimatex[i] - estimatex[i-1] - L[i])/m[i]
		    			+ k[i+1]*( estimatex[i+1] - estimatex[i] - L[i+1])/m[i];
		    }
	    
	    for(int i =0; i<n; i++){
	    	k3[i] = estimatev[i];
	    	l3[i] = a[i];
	    }
	    
	   
	    for(int i = 0; i<n; i++) {
	      x[i] += h * (k0[i] + 2*k1[i] + 2*k2[i] + k3[i])/6.0;
	      v[i] += h * (l0[i] + 2*l1[i] + 2*l2[i] + l3[i])/6.0;
	    }
	    
		// Določitev koordinat za risanje vzmeti.
	    int[] xkoordinate = {0,0};
	    
	    xkoordinate[0] = (int) (width * stena1);
		xkoordinate[1] = (int) (width * x[0]);
		// Barva vzmeti.
		g.setColor(Color.BLUE);
		
		int razmik = xkoordinate[1]-xkoordinate[0];
		
		int [] xAxis = {xkoordinate[0],0,0,0,0,0,0,0,0,0,0,xkoordinate[1]};
		for (int kk = 1; kk<11; kk++){
			xAxis[kk] = xkoordinate[0] + razmik/12*kk;
		}
		
		int [] yAxis = {(int)height/2,0,0,0,0,0,0,0,0,0,0, (int)height/2};
		for (int kk = 1; kk<11; kk++){
			if (kk%2==0){
				yAxis[kk] = (int)height/2  + 10;
			}
			else{
				yAxis[kk] = (int)height/2 - 10;
				}
		}
		// Risanje skrajne leve vzmeti.
		if(leva){
		g.drawPolyline(xAxis, yAxis, 12);   
		}
		
		// Barva stene.
		g.setColor(Color.BLACK);
		
		// Risanje desne stene.
		if(desna){
		g.drawLine((int) (width * stena2),  (int) height/2 - 100, (int) (width * stena2),  (int)height/2+100);
		}
		
		// Risanje leve stene.
		if(leva){
		g.drawLine((int) (width * stena1),  (int) height/2 - 100, (int) (width * stena1),  (int)height/2+100);
		}
		
		
		for (int k = 0; k<n; k++){
			//System.out.println(12);
			int j = height / 2;
			int i = (int) (width * x[k]);
			int r = 5;
			g.setColor(Color.GREEN);
			g.fillOval(i-r, j-r, 2 * r, 2 * r);	
			
			g.setColor(Color.BLUE);
			if (k == n-1){
				xkoordinate[0] = (int) (width * x[n-1]);
				xkoordinate[1] = (int) (width * stena2);
			}
			
			else{
				xkoordinate[0] = (int) (width * x[k]);
				xkoordinate[1] = (int) (width * x[k+1]);
			}
			
			razmik = xkoordinate[1] - xkoordinate[0];
			
			xAxis[0] = xkoordinate[0];
			xAxis[11] = xkoordinate[1];
			for (int kk = 1; kk<11; kk++){
				xAxis[kk] = xkoordinate[0] + razmik/12*kk;
			}
			
			for (int kk = 1; kk<11; kk++){
				if (kk%2 == 0){
					yAxis[kk] = j  + 10;
				}
				else{
					yAxis[kk] = j - 10;
					}
			}
			
			// Risanje vmesnih vzmeti.
			if (k < n-1 )g.drawPolyline(xAxis, yAxis, 12);
			
			// Risanje skrajne desne vzmeti.
			if (k == n-1)
			{
			 if (desna) g.drawPolyline(xAxis, yAxis, 12);   
			}
		}
		
        
       
	}
	//
}