import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.awt.Checkbox;

import javax.swing.JOptionPane;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;




public class Konzola extends JFrame implements ActionListener {
	// Definicija spremenljivk
	private JTextField dolzina;
	private JTextField masa;
	private JTextField koeficient;
	private JTextField stevilo;
	private Checkbox leva_stena;
	private Checkbox desna_stena;
	
	// Definicija konzole in gumbov.
	private static final long serialVersionUID = 1L;
	KonzolaPanel fireworksPanel = new KonzolaPanel();
	JFrame frame = new JFrame("Konzola");
	JButton actionButton=new JButton("Poženi");
	JButton resetButton = new JButton("Začni znova");	
	boolean animating = false;
	Thread animationThread = null;
	// Klic timerja, ki omogoča simulacijo.
	Timer timer = new Timer(40, this);
	   
	private void addMenu(JFrame frame) {
		// Nastavitev menija.
		JMenu file = new JMenu("File");
		file.setMnemonic('F');
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.setMnemonic('x');
		exitItem.addActionListener(this);
		file.add(exitItem);
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(file);
		frame.setJMenuBar(menuBar);	
	}
	
 
    public void addComponentsToPane(Container pane) {   
    	// Postavitev elementov v okencu
    pane.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    
    
  // Gumb, ki požene simulacijo.
    c = new GridBagConstraints();
    c.weightx = 0.5;
	c.gridx = 0;
	c.gridy = 0;
	c.ipadx = 70;
    pane.add(actionButton, c);
 
    // Gumb, ki resetira simulacijo
    c = new GridBagConstraints();
	c.weightx = 1;
	c.gridwidth = 2;
	c.gridx = 1;
	c.gridy = 0;
	c.insets = new Insets(100, 15, 2, 0);
    pane.add(resetButton, c);
 
 // Nastavitev koeficienta.
 		c = new GridBagConstraints();
 		c.fill = GridBagConstraints.HORIZONTAL;
 		c.ipady = 8;       
 		c.ipadx = 40;
 		c.weightx = 0.0;
 		c.gridwidth = 3;
 		c.gridx = 0;
 		c.gridy = 1;
 		pane.add(new JLabel("Vnesi koeficient vzmeti: "), c);
 		
 // Polje za vnos koeficienta
 		c = new GridBagConstraints();
 		c.fill = GridBagConstraints.HORIZONTAL;
 		c.ipady = 8;       
 		c.weightx = 0.0;
 		c.gridwidth = 3;
 		c.gridx = 1;
 		c.gridy = 1;
 		koeficient = new JTextField("1",5);
	 		pane.add(koeficient, c);
    
 	// Nastavitev mase
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipady = 8;      
 	 		c.ipadx = 40;
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 0;
 	 		c.gridy = 2;
 	 		pane.add(new JLabel("Vnesi maso vzmeti: "), c);
 	 		
 	 //Polje za vnos mase
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipady = 8;       
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 1;
 	 		c.gridy = 2;
 	 		masa = new JTextField("0.5",5);
 	 		//dolzina.setText("0");
 	 		pane.add(masa, c);
 	 		
 	 		
 	 	// Nastavitev ravnovesne dolžine vzmeti
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipadx = 40;
 	 		c.ipady = 8;      
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 0;
 	 		c.gridy = 3;
 	 		pane.add(new JLabel("Vnesi dolzino vzmeti: "), c);
 	 		
 	 		// Polje za vnos ravnovesne dolžine
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipady = 8;       
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 1;
 	 		c.gridy = 3;
 	 		dolzina = new JTextField("0.17",5);
 	 		pane.add(dolzina, c);
 		
 	 		
 	 	// Nastavitev števila kroglic.
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipadx = 40;
 	 		c.ipady = 8;       
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 0;
 	 		c.gridy = 4;
 	 		pane.add(new JLabel("Vnesi stevilo kroglic: "), c);
 	 		
 	 		//Polje za vnos števila kroglic
 	 		c = new GridBagConstraints();
 	 		c.fill = GridBagConstraints.HORIZONTAL;
 	 		c.ipady = 8;       
 	 		c.weightx = 0.0;
 	 		c.gridwidth = 3;
 	 		c.gridx = 1;
 	 		c.gridy = 4;
 	 		stevilo = new JTextField("10",5);
 	 		pane.add(stevilo, c);
 	 		
 	 		// Nastavitev levega vpetja.
 	 		leva_stena = new Checkbox("Vpetje v levo steno", true);
 	 		pane.add(leva_stena);
 	 		
 	 		// Nastavitev desnega vpetja.
 	 		desna_stena = new Checkbox("Vpetje v desno steno", true);
 	 		pane.add(desna_stena);
 	 		
 	 		
 		
 		// Postavitev simulacijo.
 		c.fill = GridBagConstraints.HORIZONTAL;
 		c.ipady = 0;        
 		c.weighty = 2.0;    
 		c.anchor = GridBagConstraints.PAGE_END;  
 		c.insets = new Insets(10,0,0,0);   
 		c.gridx = 1;        
 		c.gridwidth = 1;    
 		c.gridy = 5;        
 		pane.add(fireworksPanel, c);
 		
 		
    // Dodamo funkcije obema gumboma
    actionButton.addActionListener(this);
	resetButton.addActionListener(this);
    }
    
    
	private void createAndShowGUI() {

		//Ustvari in pokaži okence.
		addMenu(frame);
		
		//Postavi platno.
		addComponentsToPane(frame.getContentPane());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fireworksPanel.setPreferredSize(new Dimension(900, 400));
		fireworksPanel.setBackground(Color.WHITE);
		
		
		//Prikaži okence.
		frame.pack();
		frame.setVisible(true);
		
		
	     
		
		
	}
	public static void main(String[] args) {
		Konzola fireworks = new Konzola();
		fireworks.createAndShowGUI();
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		
		double L = Double.parseDouble(dolzina.getText().trim());
		fireworksPanel.getDolzina(L);
		
		 
		double m = Double.parseDouble(masa.getText().trim());
		fireworksPanel.getMasa(m);
		
	
		double k = Double.parseDouble(koeficient.getText().trim());
		fireworksPanel.getKoeficient(k);
		
		
		int n = Integer.parseInt(stevilo.getText().trim());
		fireworksPanel.getStevilo(n);
		
		boolean leva = leva_stena.getState();
		fireworksPanel.getLeva(leva);
		
		boolean desna = desna_stena.getState();
		fireworksPanel.getDesna(desna);
		
		
		if (e.getActionCommand() != null) {
			//
			// Gleda, če je uporabnik izbral exit
				// iz file menija in zapre, če je
			if (e.getActionCommand().equals("Exit")) {
					System.exit(0);
				}
			Object source = e.getSource();
			if (source == actionButton)
			{				
				// Gleda, če je kliknjen gumbg Poženi, če je,
				// požene animacijo in nastavi gumb na Ustavi
				if (e.getActionCommand().equals("Poženi")) {
					fireworksPanel.reset();
					animating = true;
					timer.start();
					actionButton.setText("Ustavi");					
				}
				// Če je kliknjen Ustavi, 
				// ustavi animacijo in nastavi gumb na Naprej
				if (e.getActionCommand().equals("Ustavi")) {
					animating = false;
					timer.stop();
					actionButton.setText("Naprej");
				}
				if (e.getActionCommand().equals("Naprej")) {
					animating = true;
					timer.start();
					actionButton.setText("Ustavi");
				}
			}
			else if (source == resetButton){
				if (e.getActionCommand().equals("Začni znova")) {
					fireworksPanel.reset();
					fireworksPanel.repaint();
					animating = false;
					actionButton.setText("Poženi");
				}
			}
				
		}
		// Nariši naslednjo sliko animacije.
		if (animating){
			fireworksPanel.repaint();
		}
	}
}
